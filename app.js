function validateForm(event) {
  const firstNameErrMsg = document.getElementById('firstname-errmsg');
  const lastNameErrMsg = document.getElementById('lastname-errmsg');
  const streetErrMsg = document.getElementById('street-errmsg');
  const cityErrMsg = document.getElementById('city-errmsg');
  const stateErrMsg = document.getElementById('state-errmsg');
  const zipcodeErrMsg = document.getElementById('zipcode-errmsg');
  const emailAddressErrMsg = document.getElementById('emailaddress-errmsg');
  const creditCardErrMsg = document.getElementById('creditcard-errmsg');
  const expMonthErrMsg = document.getElementById('expmonth-errmsg');
  const expYearErrMsg = document.getElementById('expyear-errmsg');
  const securityErrMsg = document.getElementById('security-errmsg');

  const firstName = document.getElementById('firstname');
  const lastName = document.getElementById('lastname');
  const streetAddress = document.getElementById('streetaddress');
  const city = document.getElementById('city');
  const state = document.getElementById('state');
  const zipcode = document.getElementById('zipcode');
  const email = document.getElementById('email');
  const creditCardNumber = document.getElementById('creditcardnumber');
  const expMonth = document.getElementById('exp-month');
  const expYear = document.getElementById('exp-year');
  const securityCode = document.getElementById('security-code');

  if (firstName.value === '') {
    firstNameErrMsg.classList.remove('hide');
    firstNameErrMsg.innerText = 'Please enter your First name';
    firstName.focus();
  } else if (lastName.value === '') {
    firstNameErrMsg.classList.add('hide');
    lastNameErrMsg.classList.remove('hide');
    lastNameErrMsg.innerText = 'Please enter your Last name';
    lastName.focus();
  } else if (streetAddress.value === '') {
    lastNameErrMsg.classList.add('hide');
    streetErrMsg.classList.remove('hide');
    streetErrMsg.innerText = 'Please enter your Street address';
    streetAddress.focus();
  } else if (city.value === '') {
    streetErrMsg.classList.add('hide');
    cityErrMsg.classList.remove('hide');
    cityErrMsg.innerText = 'Please enter your City name';
    city.focus();
  } else if (state.value == '') {
    cityErrMsg.classList.add('hide');
    stateErrMsg.classList.remove('hide');
    stateErrMsg.innerText = 'Please enter your State';
    streetAddress.focus();
  } else if (zipcode.value === '') {
    cityErrMsg.classList.add('hide');
    stateErrMsg.classList.add('hide');
    zipcodeErrMsg.classList.remove('hide');
    zipcodeErrMsg.innerText = 'Please enter your Zip code';
    zipcode.focus();
  } else if (email.value === '') {
    zipcodeErrMsg.classList.add('hide');
    emailAddressErrMsg.classList.remove('hide');
    emailAddressErrMsg.innerText = 'Please enter your Email address';
    email.focus();
  } else if (creditCardNumber.value === '') {
    emailAddressErrMsg.classList.add('hide');
    creditCardErrMsg.classList.remove('hide');
    creditCardErrMsg.innerText = 'Please enter your Credit card number';
    creditCardNumber.focus();
  } else if (expMonth.value === '') {
    creditCardErrMsg.classList.add('hide');
    expMonthErrMsg.classList.remove('hide');
    expMonthErrMsg.innerText = 'Please enter your Credit card expiration month';
    expMonth.focus();
  } else if (expYear.value === '') {
    expMonthErrMsg.classList.add('hide');
    expYearErrMsg.classList.remove('hide');
    expYearErrMsg.innerText = 'Please enter your Credit card expiration year';
    expYear.focus();
  } else if (securityCode.value === '') {
    expYearErrMsg.classList.add('hide');
    securityErrMsg.classList.remove('hide');
    securityErrMsg.innerText = 'Please enter your CVC';
    securityCode.focus();
  } else {
    securityErrMsg.classList.add('hide');

    return true;
  }
  event.preventDefault();
}
